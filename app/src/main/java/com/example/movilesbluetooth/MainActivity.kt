package com.example.movilesbluetooth

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.widget.Button
import android.widget.Toast
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //val BTAdapter = BluetoothAdapter.getDefaultAdapter();

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)

        var btnDatabase : Button = findViewById(R.id.btnCheck)

        var btnBluetooth : Button = findViewById(R.id.btnBluetooth)

        btnBluetooth.setOnClickListener {
            Toast.makeText(this@MainActivity,"Entered Discovery Mode", Toast.LENGTH_SHORT).show()

            val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            startActivityForResult(enableBluetooth,0) }

        var currentTime = LocalDateTime.now()
        var formatter = DateTimeFormatter.ofPattern ("d-M-YYYY");
        var formatted = currentTime.format(formatter)
        var pastMonth = formatted.split("-").get(1)
        var newMonth = pastMonth.toInt()-1
        var chain = formatted.split("-")[0]+"-"+newMonth+"-"+formatted.split("-")[2]


        btnDatabase.setOnClickListener {
            //AsyncTask.execute {
            try {

                URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + chain + "/students/201224929").openStream().bufferedReader().use{ it.readText()}
                mostrarMensaje(true)
                //Toast.makeText(this@MainActivity, "Found in the List",Toast.LENGTH_LONG)
            }
            catch(e:Exception) {
                //Toast.makeText(this@MainActivity, "Not found in the List",Toast.LENGTH_LONG)
                mostrarMensaje(false)            }
            }
        //}
    }


    fun mostrarMensaje(condicion:Boolean)
    {
        if(condicion)
        {
            Toast.makeText(applicationContext, "Se encontró", Toast.LENGTH_LONG).show()
        }
        else
        {
            Toast.makeText(applicationContext, "    NO se encontró", Toast.LENGTH_LONG).show()
        }

    }
}
